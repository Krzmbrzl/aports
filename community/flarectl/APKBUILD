# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=flarectl
pkgver=0.49.0
pkgrel=0
pkgdesc="CLI application for interacting with a Cloudflare account"
url="https://github.com/cloudflare/cloudflare-go/tree/master/cmd/flarectl"
arch="all"
license="BSD-3-Clause"
makedepends="go"
source="https://github.com/cloudflare/cloudflare-go/archive/refs/tags/v$pkgver/flarectl-$pkgver.tar.gz"
builddir="$srcdir/cloudflare-go-$pkgver/cmd/flarectl"
options="!check chmod-clean" # no testsuite provided

export GOCACHE="$srcdir/go-cache"
export GOTMPDIR="$srcdir"
export GOMODCACHE="$srcdir/go"
export GOFLAGS="$GOFLAGS -buildvcs=false"

build() {
	go build -o flarectl
}

package() {
	install -Dm755 flarectl -t "$pkgdir"/usr/bin
}

sha512sums="
5899847c8fff7a1a6dc9d305b26e32aa0f0cbdfe2f7487f4a4d0c37083d86f7a312f8535c32e1b7f24362e132a48f2e4dcc38ad950bc306880d8fb94488323ef  flarectl-0.49.0.tar.gz
"
