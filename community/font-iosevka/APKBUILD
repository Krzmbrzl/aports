# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=16.1.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
	"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
747a4d9a9374b1db27a7b53a1c4efd410e447976c83cc487ed550235e1a8fec5051339561dfd4a984e8a93c213f121defd7b27e3e6e561c0c6c715ad913a8635  super-ttc-iosevka-16.1.0.zip
14af913c2d1ddcf8744e6e8949b2abc46eca55af5e6062cf46a1551dab65ea61862c4cba3b3e08b3bb8eee67cc61c943f4c9c242f2678af7e7cf808b0c498ea2  super-ttc-iosevka-slab-16.1.0.zip
b0cfe0223bfbbb9b942685c7ddc628865ec09d5f571613d0079c66a4b3bb45825471cecedcec4d280e52a5133289524fc34cce95258b2e771e54de1be2d53762  super-ttc-iosevka-curly-16.1.0.zip
aa856c97c0d7620031796287f2ea545d20179527c096caface0d4260ba01f27d35a790c3a11a3d7bc60447fcffef53b03d51678c6008160dcfc029f1d737a1fb  super-ttc-iosevka-curly-slab-16.1.0.zip
"
